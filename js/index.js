// 封装 querySelector
function $(element) {
    return document.querySelector(element)
}

// 获取左侧区域元素
const dragBox = document.getElementById('dragBox')

// 获取左侧区域标题元素
const title = document.getElementById('title')

// 获取右侧区域内容元素
const content = document.getElementById('content')

// 获取右侧区域页码元素
const pagination = document.getElementById('pagination')

// 获取右侧区域底部页码按钮元素
let btnNum = null

// API接口
const API_URL = 'https://b.amujun.com/upload_file_ocr'

// 参数
let params = {}

// 定义支持的文件类型
const fileType = ['jpg', 'png', 'jpeg', 'bmp', 'pdf']

// 返回的数据
let result = []

// 鼠标拖动的对象进入其容器范围内时触发此事件
dragBox.addEventListener('dragenter', () => {
    dragBox.className = 'left frame'
})

/**
 * 鼠标拖动的对象在另一对象容器范围内拖动时触发此事件
 * 进入子集的时候 会触发ondragover 频繁触发 不给ondrop机会
 */
dragBox.addEventListener('dragover', event => event.preventDefault())

// 在一个拖动过程中，释放鼠标键时触发此事件
dragBox.addEventListener('drop', event => {
    event.preventDefault()
    dragBox.className = 'left'
    packageDrop(event)
})

// 当被鼠标拖动的对象离开其容器范围内时触发此事件
dragBox.addEventListener('dragleave', () => {
    dragBox.className = 'left'
})

// 在全局执行粘贴行为触发此事件
window.addEventListener('paste', event => {
    // 阻止触发默认的粘贴事件
    event.preventDefault()
    packageDrop(event)
})

// 进入下一页
function nextPage(val) {
    // 初始化底部所有页码元素样式
    for (const element of btnNum) {
        element.className = 'btnNum'
    }
    // 拿到当前索引的内容
    let textContent = result.content[val.innerText - 1]
    // 将当前索引的内容展示在页面中
    content.innerText = textContent
    // 如果内容都一样，页码按钮就显示激活状态
    if (content.innerText == textContent) {
        val.className = 'btnNum active'
    }
}
