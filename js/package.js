let packageDrop = event => {
    let { files } = event.dataTransfer || event.clipboardData
    let dropFiles = [] // 存放的文件对象
    for (const item of files) {
        // 判断文件类型
        for (const i of fileType) {
            // 判断是否符合上传类型
            if (item.type.includes(i)) {
                dropFiles.push(item)
            }
        }
    }
    // 如果没有文件
    if (dropFiles.length === 0) {
        return alert('不支持该文件类型，请重新上传')
    } else if (dropFiles.length > 1) {
        // 判断是否只有一个文件
        return alert('暂时只支持单个文件上传')
    }
    // 拿到文件
    params.file = dropFiles[0]
    // 判断是否是PDF文件
    if (dropFiles[0].type.includes('pdf')) {
        params.type = 'pdf'
        params.page = 1
    } else {
        // 不是PDF文件，则一定为图片
        params.type = 'image'
    }

    // 开始上传
    title.innerText = '正在上传文件，请稍等...'
    let data = new FormData()
    data.append('file', dropFiles[0])
    data.append('type', params.type)
    if (params.type === 'pdf') {
        data.append('page', params.page)
    }
    let xhr = new XMLHttpRequest()
    xhr.addEventListener('readystatechange', function () {
        // 判断请求是否已完成
        if (this.readyState === 4) {
            title.innerText = '拖拽图片或PDF文件到此处'
            // 返回的数据,responseText 接口返回的内容
            result = JSON.parse(this.responseText)

            // 创建文件读取对象
            let reader = new FileReader()
            // 读取文件
            reader.readAsDataURL(dropFiles[0])
            if (params.type.startsWith('image')) {
                // 监听onload事件
                reader.onload = function () {
                    $('.left .title').className = 'hide'
                    $('.left .introduce').className = 'hide'
                    $('.left .image').src = this.result
                }
            } else if (params.type === 'pdf') {
                // 监听onload事件
                reader.onload = function () {
                    $('div.tips').className = 'hide'
                    $('div#example1').className = ''
                    PDFObject.embed(this.result, '#example1')
                }
            }
            // 默认展示第一页的内容
            content.innerText = result.content[0]
            // 获取分页模板
            pagination.innerHTML = template('artTemplate', { result /* 传递给模板的数据 */ })
            btnNum = document.querySelectorAll('.btnNum')
            // 默认选择第一页
            btnNum[0].className = 'btnNum active'
            // 初始化底部页码元素类名
            pagination.className = 'pagination'
        }
    })
    xhr.open('POST', API_URL)
    xhr.send(data)
}
